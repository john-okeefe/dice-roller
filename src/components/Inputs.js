import React from 'react';
import { Row } from 'reactstrap';
import InputLong from './InputLong';
import InputShort from './InputShort';
import DiceCountAlert from './DiceCountAlert'
import '../styles/Inputs.css'

export default (props) => (
    <div>
        <div className="d-flex flex-row justify-content-center">{props.dcAlert ? <DiceCountAlert /> : null }</div>
        <Row className="d-flex flex-sm-row my-flex-container justify-content-around input-container">
            {props.inputLong ?
                <InputLong
                    diceCount={props.diceCount}
                    diceType={props.diceType}
                    modifier={props.modifier}
                    onDiceCountChange={props.onDiceCountChange}
                    onDiceTypeChange={props.onDiceTypeChange}
                    onModifierChange={props.onModifierChange}
                    captureDiceRoll={props.captureDiceRoll}
                    inputType={props.modifier}
                    onDiceCountUp={props.onDiceCountUp}
                    onDiceCountDown={props.onDiceCountDown}
                /> :
                <InputShort
                    input={props.input}
                    onInputChange={props.onInputChange}
                    captureDiceRoll={props.captureDiceRoll}
                />}

        </Row>
    </div>
)