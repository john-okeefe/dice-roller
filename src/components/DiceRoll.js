import React from 'react';
import { Fade, Row } from 'reactstrap';

export default (props) => (
    <Row>
        {props.roll.map((x, i) => {
                const first = i === 0 ? 'first' : '';
                return (
                    <Fade key={i*5} in={props.fadeIn} className={"text-center " + first + " col-auto"}>
                        <div className="dice-num">#{i+1}</div>
                        <div className="lead">{x}</div>
                    </Fade>
                )
            }

        )}
    </Row>
)