import React from 'react';

export default () => (
    <div className="alert alert-danger">
        Please enter a Dice Count in between 1 and 1000
    </div>
)