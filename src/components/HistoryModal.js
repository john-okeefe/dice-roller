import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default (props) => (
    <Modal isOpen={props.modal} toggle={props.modalToggle} className={props.className}>
        <ModalHeader toggle={props.modalToggle}>Individual Dice</ModalHeader>
        <ModalBody>
            <div>Modifier: {props.clickedModifierHistory}</div>
            {props.clickedHistory.map((dice, count) => <div key={count}>Dice #{count+1}: {dice}</div>)}
        </ModalBody>
        <ModalFooter>
            <Button color="primary" onClick={props.modalToggle}>Close</Button>
        </ModalFooter>
    </Modal>
)