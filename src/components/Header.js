import React from 'react';
import '../styles/Header.css';

export default (props) => (
    <div className="d-flex flex-row">
        <div className="mr-auto p-2">
            <h1 className="float-left">Dice Roller</h1>
        </div>
        <div className="p-2">
            {props.inputLong ? <button type="button" className="btn btn-info" onClick={props.handleClick}>Text Input</button> : <button type="button" className="btn btn-info float-right" onClick={props.handleClick}>Multi-Box Input</button>}
        </div>
        <div className="p-2">
            <button type="button" className="btn btn-info" onClick={props.reset}>Reset All</button>
        </div>
    </div>
)