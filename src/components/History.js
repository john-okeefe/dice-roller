import React from 'react';
import {Button} from 'reactstrap';
import '../styles/History.css';

export default (props) => (
    <div>
        <h2>History</h2>
        <div className="d-flex align-content-start flex-wrap">
            {props.history.map((rollArr, index) => {
                const modifier = props.modifierHistory[index];
                return (
                    <div key={index * 6}>
                        <Button className="historyButtons" color="info" size="lg"
                                onClick={() => props.modalToggle(rollArr, modifier)}>
                            {rollArr.reduce((acc, curr) => acc + curr) + Number(modifier)}</Button>
                    </div>
                )
            })}
        </div>
    </div>
)