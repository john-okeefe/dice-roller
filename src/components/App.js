import React from 'react';
import '../styles/App.css';
import Header from './Header';
import Inputs from './Inputs'
import CurrentRoll from './CurrentRoll';
import HistoryModal from './HistoryModal';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            diceCount: '1',
            diceType: 'd20',
            modifier: '0',
            roll: [],
            history: [],
            modifierHistory: [],
            finalCount: '',
            input: '1d20m0',
            inputLong: true,
            columns: 12,
            fadeIn: true,
            modal: false,
            clickedModifierHistory: '',
            clickedHistory: [],
            dcAlert: false
        }
    }

    reset = () => {
        this.setState(() => ({
            diceCount: '1',
            diceType: 'd20',
            modifier: '0',
            roll: [],
            history: [],
            modifierHistory: [],
            finalCount: '',
            input: '1d20m0',
            inputLong: true,
            columns: 12,
            fadeIn: true,
            modal: false,
            clickedModifierHistory: '',
            clickedHistory: []
        }))
    };

    onDiceCountChange = (e) => {
        const diceCount = e.target.value;
        const diceType = this.state.input.match(/d\d+/g);
        const mod = this.state.input.match(/m-?\d+/g);
        const input = diceCount + diceType + mod;
        this.setState(() => ({
            diceCount,
            input
        }));
    };

    onDiceTypeChange = (e) => {
        const diceType = e.target.value;
        const diceCount = this.state.input.match(/^\d+/g);
        const mod = this.state.input.match(/m-?\d+/g);
        const input = diceCount + diceType + mod;
        this.setState(() => ({
            diceType,
            input
        }))
    };

    onModifierChange = (e) => {
        const modifier = e.target.value;
        const diceCount = this.state.input.match(/^\d+/g);
        const diceType = this.state.input.match(/d\d+/g);
        const newMod = 'm' + modifier;
        const input = diceCount + diceType + newMod;
        this.setState(() => ({
            modifier,
            input
        }))
    };

    onDiceCountUp = () => {
        const diceCount = (Number(this.state.diceCount) + 1).toString();
        const input = this.state.input.split('').map((x, i) => i === 0 ? diceCount : x).join('');
        this.setState(() => ({
            diceCount,
            input
        }))
    };

    onDiceCountDown = () => {
        const diceCount = (Number(this.state.diceCount) - 1).toString();
        const input = this.state.input.split('').map((x, i) => i === 0 ? diceCount : x).join('');
        if (this.state.diceCount > 1) {
            this.setState(() => ({
                diceCount,
                input
            }))
        }
    };

    onInputChange = (e) => {
        const input = e.target.value;
        const diceCount = input.match(/^\d+/g);
        const diceType = input.match(/d\d+/g);
        const mod = input.match(/m-?\d+/g);
        this.setState(() => ({
            input,
            diceCount: diceCount ? diceCount[0] : '1',
            diceType: diceType ? diceType[0] : 'd20',
            modifier: mod ? mod[0].substr(1) : '0'
        }))
    };


    captureDiceRoll = (e) => {
        e.preventDefault();
        this.setState(() => ({fadeIn: false}));
        const diceCount = this.state.diceCount;
        const diceType = this.state.diceType;
        const diceArr = [];
        const minDiceAmount = 1;
        const maxDiceAmount = 1000;

        if(diceCount < minDiceAmount || diceCount > maxDiceAmount){//move hard coded numbers to const variable for readability
            this.setState(()=>({dcAlert: true}))
        }else{
            // setTimeout(() => {
            for (let i = 0; i < diceCount; i++) {
                const number = Number(diceType
                    .split("")
                    .splice(1, 3)
                    .join(""));
                const roll = Math.floor(Math.random() * number + 1);
                diceArr.push(roll);
            }
            const finalCount = (diceArr.reduce((prev, curr) => prev + curr)) + Number(this.state.modifier);
            // this.columnSolution(diceCount);
            this.setState((prevState) => ({
                roll: diceArr,
                history: [...prevState.history, diceArr],
                modifierHistory: [...prevState.modifierHistory, this.state.modifier],
                finalCount,
                fadeIn: true,
                dcAlert: false
            }));
            // }, 500)
        }
    };

    modalToggle = (clickedHistory, clickedModifierHistory) => {
        if (clickedHistory) {
            this.setState(() => ({
                modal: !this.state.modal,
                clickedHistory,
                clickedModifierHistory
            }))
        } else {
            this.setState(() => {
                modal: !this.state.modal
            })
        }
    };

    handleClick = () => {
        const diceCount = this.state.diceCount;
        const diceType = this.state.diceType;
        const modifier = this.state.modifier;
        const input = diceCount + diceType + 'm' + modifier;
        this.state.inputLong ? this.setState(() => ({
            inputLong: false,
            input
        })) : this.setState(() => ({inputLong: true}))
    };

    columnSolution = (diceCount) => {
        let columns = 1;
        if (diceCount < 12) {
            columns = Math.round(12 / diceCount)
        } else {
            let diceLessTwelve = diceCount;
            for (let i = 1; i < Math.ceil(diceCount / 12); i++) {
                diceLessTwelve = diceLessTwelve - 12
            }
            columns = diceLessTwelve
        }
        this.setState(() => ({columns}));
    };

    render() {
        return <div className='container app'>
            <Header
                inputLong={this.state.inputLong}
                handleClick={this.handleClick}
                reset={this.reset}
            />
            <Inputs
                inputLong={this.state.inputLong}
                diceCount={this.state.diceCount}
                diceType={this.state.diceType}
                modifier={this.state.modifier}
                onDiceCountChange={this.onDiceCountChange}
                onDiceTypeChange={this.onDiceTypeChange}
                onModifierChange={this.onModifierChange}
                captureDiceRoll={this.captureDiceRoll}
                inputType={this.state.modifier}
                onDiceCountUp={this.onDiceCountUp}
                onDiceCountDown={this.onDiceCountDown}
                input={this.state.input}
                onInputChange={this.onInputChange}
                dcAlert={this.state.dcAlert}
            />
            <CurrentRoll
                columns={this.state.columns}
                roll={this.state.roll}
                finalCount={this.state.finalCount}
                modifier={this.state.modifier}
                fadeIn={this.state.fadeIn}
                history={this.state.history}
                modal={this.state.modal}
                modalToggle={this.modalToggle}
                className={this.props.className}
                modifierHistory={this.state.modifierHistory}
            />
            {this.state.modal ?
                <HistoryModal
                    modal={this.state.modal}
                    modalToggle={this.modalToggle}
                    className={this.props.className}
                    clickedHistory={this.state.clickedHistory}
                    clickedModifierHistory={this.state.clickedModifierHistory}
                /> : null}
        </div>;
    }
}