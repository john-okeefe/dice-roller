import React from 'react';
import {Form} from 'reactstrap';
import '../styles/InputShort.css';

export default (props) => (
    <Form inline onSubmit={props.captureDiceRoll}>
        <div className="p-2">
            <input
                type="text"
                placeholder="Dice roll"
                autoFocus
                value={props.input}
                onChange={props.onInputChange}
            />
        </div>
        <div className="p-2">
            <span className="lead example-text">ex. 1d20m2</span>
        </div>
        <div className="p-2">
            <button className="btn btn-light">Submit</button>
        </div>
    </Form>
)