import React from 'react';
import DiceRoll from './DiceRoll'
import History from './History'
import '../styles/CurrentRoll.css';
import {Row, Col} from 'reactstrap';

export default (props) => (
    <div className="container">
        <Row>
            <Col sm="8">
                <Row className="total-color">
                    <Col className="modifier-view">
                        <span
                            className="lead">Modifier: {props.modifier > 0 ? `+${props.modifier}` : props.modifier}</span>
                    </Col>
                    <Col>
                        <div className="text-center total-view"><strong>{props.finalCount}</strong></div>
                    </Col>
                    <Col>
                        <span className="lead">Total: {props.finalCount ? props.finalCount : '0'}</span>
                    </Col>
                </Row>
                <DiceRoll
                    roll={props.roll}
                    columns={props.columns}
                />
            </Col>
            <Col sm="4">
                <History
                    history={props.history}
                    modal={props.modal}
                    modalToggle={props.modalToggle}
                    className={props.className}
                    modifierHistory={props.modifierHistory}
                    modifier={props.modifier}
                />
            </Col>
        </Row>
    </div>
)