import React from "react";
import Icon from "react-icons-kit";
import {
    Button,
    Form,
    InputGroup,
    InputGroupAddon,
    Input
} from "reactstrap";
import { circleUp } from "react-icons-kit/icomoon/circleUp";
import { circleDown } from "react-icons-kit/icomoon/circleDown";
import '../styles/InputLong.css'

export default props => (
    <Form inline onSubmit={props.captureDiceRoll}>
        <div className="p-2 dice-count-input">
            <InputGroup>
                <InputGroupAddon addonType="prepend">
                    <Button className="btn btn-light" onClick={props.onDiceCountDown}>
                        <Icon icon={circleDown} />
                    </Button>
                </InputGroupAddon>
                <Input
                    type="text"
                    placeholder="How many?"
                    className="form-control"
                    autoFocus
                    value={props.diceCount}
                    onChange={props.onDiceCountChange}
                />
                <InputGroupAddon addonType="append">
                    <Button className="btn btn-light" onClick={props.onDiceCountUp}>
                        <Icon icon={circleUp} />
                    </Button>
                </InputGroupAddon>
            </InputGroup>
        </div>
        <div className="p-2">
            <InputGroup>
                <select
                    className="custom-select"
                    value={props.diceType}
                    onChange={props.onDiceTypeChange}
                >
                    <option value="d4">d4</option>
                    <option value="d6">d6</option>
                    <option value="d8">d8</option>
                    <option value="d10">d10</option>
                    <option value="d12">d12</option>
                    <option value="d20">d20</option>
                    <option value="d100">d100</option>
                </select>
            </InputGroup>
        </div>
        <div className="p-2 modifier-input">
            <InputGroup>
                <input
                    type="text"
                    className="form-control"
                    placeholder="Modifier"
                    value={props.modifier}
                    onChange={props.onModifierChange}
                />
            </InputGroup>
        </div>
        <div className="p-2">
            <InputGroup>
                <Button className="btn btn-light">Submit</Button>
            </InputGroup>
        </div>
    </Form>
)